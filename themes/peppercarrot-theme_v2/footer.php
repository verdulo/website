<?php if (!defined('PLX_ROOT')) exit; ?>
<footer class="footer" role="contentinfo">
    <div class="container">
        <div class="grid">
            
            <div class="col sml-12 text-center"> 
            <p>
            <a href="#top" title="Back to top"><img src="themes/peppercarrot-theme_v2/ico/top.svg" alt="+"/> TOP <img src="themes/peppercarrot-theme_v2/ico/top.svg" alt="+"/></a>
            <a href="<?php $plxShow->urlRewrite('core/admin/') ?>" rel="nofollow" >.</a>
            <p><?php $plxShow->lang('FOOTER_CONTENT') ?></p>          
            </p>
            </div>

            <br/>
        </div>
    </div>
</footer>

</body>
</html>
